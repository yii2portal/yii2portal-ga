<?php

namespace yii2portal\ga;

use Yii;
use yii\web\View;


class Module extends \yii2portal\core\Module
{

    public $identifier;

    public function init()
    {
        parent::init();
        $js = <<<EOF
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '{$this->identifier}', 'auto');
ga('send', 'pageview');
ga('require', 'displayfeatures');

EOF;
if(!Yii::$app->request->isAjax){
        Yii::$app->view->registerJs($js, View::POS_END);
    }
    }
}